<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Folder extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
			parent::__construct();
			$this->load->helper("url");
			$this->load->model("FolderList_model");
            $this->load->model("Folder_model");			
			$this->load->model("Image_model");            
			// Your own constructor code
	}


	public function index()
	{   
        $data['folders'] = $this->FolderList_model->getFoldersWithDetails();
        $this->load->view('header');
		$this->load->view('folderList', $data);
		$this->load->view('footer');
    }

    public function view($folderName)
    {
		$this->Folder_model->setFolderName($folderName);
		$data['files'] = $this->Folder_model->getFilesInFolderWithDetails();
		$data['foldername'] = $folderName;
        $this->load->view('header');
		$this->load->view('folder', $data);
		$this->load->view('footer');
		}
		
	public function createnew($folderName){
		$this->Folder_model->setFolderName($folderName);
		$this->Folder_model->createNew();
	}

	public function uploadfiles($folderName){
		$this->Folder_model->setFolderName($folderName);
		$this->Folder_model->uploadFiles();
	}

	public function changefoldername($folderName, $newFolderName)
	{
		$this->Folder_model->setFolderName($folderName);
		$this->Folder_model->changeFolderName($newFolderName);
	}

    

	public function test($folderName)
	{
		$this->Folder_model->setFolderName($folderName);
        echo FCPATH . "\n";
        var_dump($this->FolderList_model->getFolders() );

        var_dump($this->FolderList_model->getFilesInFolder() );
	}
}
