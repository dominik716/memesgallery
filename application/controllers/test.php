<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class test extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
			parent::__construct();
			$this->load->helper("url");
			$this->load->model("FolderList_model");
			$this->load->model("Folder_model");
			// Your own constructor code
	}


	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function test()
	{
        echo FCPATH . "\n";
        var_dump($this->FolderList_model->getFolders() );

        var_dump($this->Folder_model->getFilesInFolderWithDetail("pierwszyFolder") );

	}
}
