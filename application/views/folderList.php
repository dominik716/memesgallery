<?php
$url = site_url();
?>
<div class="container my-3">

    <div class="card">
        <div class="card-header primary-color text-white text-center">
            <h3 class="m-0">Wybierz katalog</h3>
        </div>
        <ul class="list-group list-group-flush">
            <?php foreach ($folders as $folder) : ?>
            <a class="text-dark" href="<?php echo $url . "folder/view/" . $folder['name']; ?>">
                <li class="list-group-item waves-effect border-bottom long-press" data-folderName="<?=str_replace("_", " ",  $folder['name']); ?>" data-long-press-delay="400">

                    <i class="far fa-folder mr-2 ml-2"></i><?php echo str_replace("_", " ",  $folder['name']); ?>
                    <small class="files-number font-smaller text-muted ml-2"><?php echo $folder['numberFiles']; ?> plików</small>
                </li>
            </a>
            <?php endforeach; ?>
        </ul>

    </div>
    <!--
<div class="text-center">
<button type="button" class="btn primary-color btn-sm m-0 my-3"  data-toggle="modal" data-target="#createNewFolderModal" >Dodaj nowy folder</button>
</div>
-->

    <div class="fixed-action-btn" style="bottom: 26px; right: 26px;">
        <button class="btn btn-xl btn-circle2 primary-color" data-toggle="modal" data-target="#createNewFolderModal">
            <i class="fas fa-plus btn-circle-font-size"></i>
        </button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="createNewFolderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document">
            <!--Content-->
            <div class="modal-content">

                <!--Header-->
                <div class="modal-header">
                    <img src="css/folder.png" class="rounded-circle img-responsive" alt="Avatar photo">
                </div>
                <!--Body-->
                <div class="modal-body text-center mb-1">

                    <h5 class="mt-1 mb-2">Stwórz nowy folder</h5>

                    <div class="md-form ml-0 mr-0">
                        <input type="text" id="folderName" class="form-control ml-0">
                        <label id="folderNameLabel" for="folderName" class="ml-0">Nazwa folderu</label>
                    </div>

                    <div class="text-center mt-4">
                        <button class="btn btn-primary waves-effect waves-light" data-dismiss="modal"
                            id="createNewFolder">Stwórz

                        </button>
                    </div>
                </div>
            </div>
        </div>
		</div>
		

		    <!-- Modal -->
				<div class="modal fade" id="changeFolderNameModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document">
            <!--Content-->
            <div class="modal-content">

                <!--Header-->
                <div class="modal-header">
                    <img src="css/folder.png" class="rounded-circle img-responsive" alt="Avatar photo">
                </div>
                <!--Body-->
                <div class="modal-body text-center mb-1">

                    <h5 class="mt-1 mb-2">Zmień nazwę folderu</h5>

                    <div class="md-form ml-0 mr-0">
                        <input type="text" id="folderNameToChange" placeholder="Nazwa folderu" data-oldFolderName="" class="form-control ml-0">
                        <label id="folderNameToChangeLabel"  for="folderName" class="ml-0">Nazwa folderu</label>
                    </div>

                    <div class="text-center mt-4">
                        <button class="btn btn-primary waves-effect waves-light" data-dismiss="modal"
                            id="changeFolderNameFinishBtn">Zmień nazwę

                        </button>
                    </div>
                </div>
            </div>
        </div>
		</div>


    <script>
    $(document).ready(function() {
        $("#Menu").append(
            '<li class="nav-item" ><a class="nav-link text-center" data-toggle="modal" data-target="#createNewFolderModal" href=>Dodaj nowy folder</a></li>'
            );


        $("#createNewFolder").on("click", function() {
            var nazwa = $("#folderName").val().replace(" ", "_");
            $.get('<?php echo $url; ?>folder/createnew/' + nazwa);
            setTimeout(function() {
                location.reload();
            }, 230);
        })

				$("#createNewFolder").on("click", function() {
            var nazwa = $("#folderName").val().replace(" ", "_");
            $.get('<?php echo $url; ?>folder/createnew/' + nazwa);
            setTimeout(function() {
                location.reload();
            }, 230);
        })

				//Gdy długo się przytrzyma nazwe folderu
        var classname = document.getElementsByClassName("long-press");
        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('long-press', function(e) {
								var nazwaFolderu = e.srcElement.dataset.foldername;
								$('#changeFolderNameModal').modal();
								$("#folderNameToChange").val(nazwaFolderu).focus().data("oldFolderName", nazwaFolderu);
                e.preventDefault();
            });
        }

				$("#changeFolderNameFinishBtn").on("click", function() {
            var newName = $("#folderNameToChange").val().replace(" ", "_");
            $.get('<?php echo $url; ?>folder/changefoldername/' + $("#folderNameToChange").data("oldFolderName") + "/" + newName);
						setTimeout(function() {
                location.reload();
            }, 230);
				})

		}); //koniec document ready

    </script>
