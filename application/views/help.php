
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header primary-color">
        <p class="heading lead">Instrukcja</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <i class="fas fa-book-open fa-4x mb-3 animated rotateIn"></i>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit iusto nulla aperiam blanditiis
            ad consequatur in dolores culpa, dignissimos, eius non possimus fugiat. Esse ratione fuga, enim,
            ab officiis totam.</p>
        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
      <button type="button" class="btn primary-color text-white m-0 my-3 waves-effect"  data-dismiss="modal" >Zamknij</button>
        
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!-- Central Modal Medium Info-->