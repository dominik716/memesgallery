<?php
$url=site_url();
?>
<!DOCTYPE html>
<html lang="en">
<link rel="manifest" href="/csharpserver/manifest.json">
<head>
  <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="theme-color" content="#3e84f4">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>APP design by Adamus</title>
  <!-- MOJE
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
  <link href="<?php echo $url;?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo $url;?>css/mdb.min.css" rel="stylesheet">
  <link href="<?php echo $url;?>css/style.css" rel="stylesheet">
  -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.6/css/mdb.min.css" rel="stylesheet">
<link href="<?php echo $url;?>css/photoswipe.css" rel="stylesheet">
<link href="<?php echo $url;?>css/default-skin/default-skin.css" rel="stylesheet">
<script src="<?php echo $url;?>js/photoswipe.min.js?v=4.1.1-1.0.4"></script> 
<script src="<?php echo $url;?>js/photoswipe-ui-default.min.js"></script> 

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript--> 
<!--
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.6/js/mdb.min.js"></script>
-->
<script src="<?php echo $url;?>js/jquery.lazy.min.js"></script> 
<script src="<?php echo $url;?>js/long-press-event.js"></script> 




</head>

<body>

<style>
html,body{
    background: rgb(248, 248, 248);
}

.btn-floating {
    -webkit-box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
    box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);
    width: 47px;
    height: 47px;
    position: relative;
    z-index: 1;
    vertical-align: middle;
    display: inline-block;
    overflow: hidden;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    margin: 10px;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    padding: 0;
    cursor: pointer;
    font-size: 1.625rem;
    line-height: 61.1px;
}

.fixed-action-btn {
    position: fixed;
    z-index: 998;
    right: 35px;
    bottom: 35px;
    margin-bottom: 0;
    padding-top: 15px;
}


.btn-circle2 {
    width: 30px;
    height: 30px;
    padding: 6px 0px;
    border-radius: 15px;
    text-align: center;
    font-size: 12px;
    line-height: 1.42857;
    color: white;
}
.btn-circle2.btn-xl {
    width: 50px;
    height: 50px;
    padding: 0px 0px 0px 0px;
    border-radius: 35px;
    font-size: 18px;
    line-height: 1.13;
}

.btn-circle-font-size{
  font-size: 1.1em!important;
}
</style>
<header>

  <nav class="navbar navbar-expand-lg navbar-dark primary-color">
    <a class="navbar-brand" href="#"><strong id="title">Galeria</strong></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto"  id="Menu">
        <li class="nav-item">
          <a class="nav-link text-center" href="<?php echo $url;?>">Lista folderów</a>
        </li>
        <li class="nav-item">
          <a class="nav-link text-center" id="help" href="#" data-toggle="modal" data-target="#help-modal" onclick="openHelp()" >Pomoc</a>
        </li>
      </ul>
      <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-center font-small" href="#">APP BY ADAMUS</a>
            </li>
      </ul>
    </div>
  </nav>

</header>

<div class="modal fade" id="help-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-notify  help-modal-content" role="document">

  </div>
</div>

<script>
function openHelp(){
    $(".help-modal-content").load("<?php echo $url;?>help/view");
}
</script>


<div id="gallery" class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>

        <div class="pswp__scroll-wrap">

        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

          <div class="pswp__ui pswp__ui--fit pswp__ui--hidden">

            <div class="pswp__top-bar">

				<div class="pswp__counter">5 / 5</div>

				<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

				<button class="pswp__button pswp__button--share pswp__element--disabled" title="Share"></button>

				<button class="pswp__button pswp__button--fs pswp__element--disabled" title="Toggle fullscreen"></button>

				<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

				<div class="pswp__preloader">
					<div class="pswp__preloader__icn">
					  <div class="pswp__preloader__cut">
					    <div class="pswp__preloader__donut"></div>
					  </div>
					</div>
				</div>
            </div>




            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap pswp__element--disabled">
	            <div class="pswp__share-tooltip">
	            </div>
	        </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption pswp__element--disabled">
              <div class="pswp__caption__center"></div>
            </div>
          </div>

        </div>


    </div>	
    


