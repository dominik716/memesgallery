<?php
$url = site_url();
?>
<style>
	.zaokraglone {
		border-radius: 10%;
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
	}

	.thumbnail {
		object-fit: cover;
		width: 100%;
		height: 100%;
	}


	.grid {
		display: grid;
		grid-template-columns: repeat(auto-fill, minmax(5rem, 1fr));
		grid-auto-rows: 1fr;
		grid-gap: 3px;
		padding: 3px;
	}

	.grid::before {
		content: '';
		width: 0;
		padding-bottom: 100%;
		grid-row: 1 / 1;
		grid-column: 1 / 1;
	}

	.grid>*:first-child {
		grid-row: 1 / 1;
		grid-column: 1 / 1;
	}
</style>

<div class="">



	<div id="demo-test-gallery " class="grid gallery" data-pswp-uid="1">
		<?php foreach ($files as $file) : ?>
			<a href="<?php echo $url . "files/" . $file['folder'] . "/" . $file['filename']; ?>" class="glowne" data-size="<?php echo $file['dimensions']; ?>">
				<img class="lazy thumbnail" data-src="<?php echo $url . "image/thumb/" . $file['folder'] . "/" . $file['filename']; ?>" alt="">
			</a>
		<?php endforeach; ?>
	</div>



</div>

<div class="fixed-action-btn" style="bottom: 26px; right: 26px;">
	<button class="btn btn-xl btn-circle2 primary-color" data-toggle="modal" data-target="#uploadFiles">
		<i class="fas fa-cloud-upload-alt btn-circle-font-size"></i>
	</button>
</div>


<!-- Modal -->
<div class="modal fade" id="uploadFiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog cascading-modal modal-avatar modal-sm" role="document">
		<!--Content-->
		<div class="modal-content">

			<!--Header-->
			<div class="modal-header">
				<img src="../../css/cloudupload.jpg" class="rounded-circle img-responsive" alt="Avatar photo">
			</div>
			<!--Body-->
			<div class="modal-body text-center mb-1">

				<h5 class="mt-1 mb-2">Przyślij pliki</h5>

				<div class="md-form ml-0 mr-0">

					<form action="<?php echo $url . "folder/uploadfiles/" . $foldername; ?>" method="post" enctype="multipart/form-data">
						<input type="file" multiple="" id="avatar" name="filesToUpload[]" accept="image/png, image/jpeg">
				</div>

				<div class="text-center mt-4">
					<button type="submit" class="btn btn-primary waves-effect waves-light" id="uploadFiles">Przyślij</button>
					</form>
				</div>
			</div>

			<script>
				$(document).ready(function() {
					$(function() {
						$('.lazy').Lazy({
							threshold: 100,
							throttle: 500
						});
					});
					$("#title").text("<?php echo str_replace("_", " ",  $foldername); ?> ");
					$("#Menu").append('<li class="nav-item" ><a class="nav-link text-center" data-toggle="modal" data-target="#uploadFiles" href=>Prześlij pliki</a></li>');


					var classname = document.getElementsByClassName("long-press");
					for (var i = 0; i < classname.length; i++) {
						classname[i].addEventListener('long-press', function(e) {
							var nazwaFolderu = e.srcElement.dataset.foldername;
							$('#changeFolderNameModal').modal();
							$("#folderNameToChange").val(nazwaFolderu).focus().data("oldFolderName", nazwaFolderu);
							e.preventDefault();
						});
					}


					//
					var divWidth = $('.glowne').width();
					$('.glowne').height(divWidth);

					$(window).resize(function() {
						$('.glowne').height(divWidth);
					});



				}); // koniec document ready







				/*
				$( document ).ready(function() {

				$("#createNewFolder").on("click", function () {
				var nazwa = $("#folderName").val().replace(" ", "_");
				$.get( '<?php echo $url; ?>folder/createnew/' + nazwa );
				setTimeout(function(){
				  location.reload();
				}, 230);
				})

				*/
			</script>
