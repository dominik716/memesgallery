<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class FolderList_model extends CI_Model
{
    public function __construct()
    {
			parent::__construct();        
    }

    public function getFolders()
    {
        $dir = new DirectoryIterator($this->config->item('files_path'));
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                $directories[] = $fileinfo->getFilename();
            }
        }
        return $directories;
    }

    /**
     * Zwraca array z tablicami ktora zawiera klucze
     * 'name ' , 'numberFiles'
     *
     * @return void
     */
    public function getFoldersWithDetails()
    {
		$folders = $this->getFolders();
		sort($folders);
        $folderArrays = [];
        foreach ($folders as $key => $folder) {
            $fi = new FilesystemIterator($this->config->item('files_path') . $folder . "/", FilesystemIterator::SKIP_DOTS);
            $folderDetail['numberFiles'] = iterator_count($fi);
            $folderDetail['name'] = $folder;
            $folderArrays[] = $folderDetail;
        }
        return $folderArrays;
    }

}
