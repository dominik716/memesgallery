<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Image_model extends CI_Model
{
    public function __construct()
    {
            //parent::__construct();  
            $this->load->helper('file');    
            $this->load->database();          
    }

    private $ImageFileName;
    private $ImageFolderName;

    public function setImage($folder, $image)
    {
        $this->ImageFolderName = $folder;
        $this->ImageFileName = $image;
    }

    public function getImageFullPath()
    {
        if(empty($this->ImageFileName)) throw new Exception("Brak podanego pliku");
        return $this->config->item('files_path') . $this->ImageFolderName . DIRECTORY_SEPARATOR .  $this->ImageFileName;
    }


    public function getThumbnail()
    {
        $cacheThumbnailPath = $this->config->item('cache_path') . $this->ImageFolderName . $this->ImageFileName;

        $config['image_library'] = 'gd2';
        $config['source_image'] =  $this->getImageFullPath();
        $config['dynamic_output'] = true;       
        $config['maintain_ratio'] = true;
        $config['width'] = $this->config->item('thumb_width');
        $config['height'] = $this->config->item('thumb_height'); 

        if($this->config->item('cache_enabled') === true)
        {
            $config['dynamic_output'] = false;   
            $config['new_image'] = $cacheThumbnailPath;
            if(!file_exists($cacheThumbnailPath))
            {
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
			} 
			
			//przekierowanie do sciezki miniaturki
			redirect($this->config->item('cache_folderName') . '/' . $this->ImageFolderName . $this->ImageFileName );exit;
           // $this->output->set_content_type(get_mime_by_extension($cacheThumbnailPath))->set_output(file_get_contents($cacheThumbnailPath))->_display();  
            return;   
        }
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    /**
     * zwraca wymiary obrazka dla photoswipe
     * string "szerokosc x dlugosc"
     *
     * @return void
     */
    public function getDimensions()
    {
        
        $query = $this->db->get_where('Files', array('Folder' => $this->ImageFolderName, 'Filename' => $this->ImageFileName));
        $row = $query->row_array();
        if(!empty($row))
        {
            return $row['DimensionPhotoSwipe'];
        }
        

        list($width, $height) = getimagesize( $this->getImageFullPath() );
        $data = array(
            'Folder' =>  $this->ImageFolderName,
            'Filename' => $this->ImageFileName,
            'DimensionPhotoSwipe' => $width."x".$height
         );    
        $this->db->insert('Files', $data);
        return $width."x".$height;
    }

    public function getWidth()
    {
        list($width, $height) = getimagesize( $this->getImageFullPath() );
        return $width;
    }
    public function getHeight()
    {
        list($width, $height) = getimagesize( $this->getImageFullPath() );
        return $height;
    }
}
