<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class Folder_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        if (!class_exists("Image_model")) {
            $this->load->model("Image_model");
            $this->load->database();
        }
    }

    private $folderName;

    /**
     * Zwraca true jeśli podany folder istnieje, w przeciwnym razie false
     *
     * @param string $foldername
     * @return bool
     */
    public function setFolderName($foldername)
    { 
        $this->folderName = $this->sanitizeFolderName($foldername);
        return $this->checkIfFolderExist();
    }
    

    public function getFolderName()
    { 
       if($this->folderName == null){
        throw new Exception("Brak podanego folderu");
       }
        return  $this->folderName;
    }
    
    /**
     * Zakończony znakiem "/"
     *
     * @return string full folder path
     */
    public function getFolderFullPath()
    {
        return $this->config->item('files_path') . $this->getFolderName() . DIRECTORY_SEPARATOR;
    }

    public function checkIfFolderExist()
    {
        return file_exists($this->getFolderFullPath());
    }

    //tworzy nowy folder jeśli nie istnieje już
    public function createNew()
    {
        return mkdir($this->getFolderFullPath());
    }


    public function getFilesInFolder()
    {
        $files = array_diff(scandir($this->getFolderFullPath()), array('..', '.'));
        return $files;
	}
	
	public function changeFolderName($newFolderName)
	{
		rename($this->config->item('files_path') . $this->getFolderName(), $this->config->item('files_path') . $newFolderName);
		return $newFolderName;
	}

	public function sanitizeFolderName($folderNameToSanitize)
	{
        foreach ($this->config->item('forbidden_symbols_foldername') as $key => $value) {
            $folderNameToSanitize = str_replace($value, "", $folderNameToSanitize);
        }
        return $folderNameToSanitize;
	}

    /**
     * Pobiera nazwe pliku i zmienia na poprawna bez kłopotliwych symboli
     * Zwraca nową nazwę pliku
     *
     * @param [type] $filename
     * @return void
     */
    public function sanitizeFileName($filename)
    {
        $fileNameNew = $filename;
        foreach ($this->config->item('forbidden_symbols_filename') as $key => $value) {
            $fileNameNew = str_replace($value, "", $fileNameNew);
        }
        return $fileNameNew;
    }

    public function renameFile($oldFilename, $newFilename)
    {
        rename($this->getFolderFullPath() . $oldFilename, $this->getFolderFullPath() . $newFilename);
    }

    public function getFilesInFolderWithDetails()
    {
        $files = array_diff(scandir($this->getFolderFullPath()), array('..', '.'));
        $filesWithDetails = [];
        foreach ($files as $key => $filename) {
            $filenameNew = $this->sanitizeFileName( $filename);
            $this->renameFile($filename, $filenameNew);      // usuwa, kolidujące z url, elementy z nazwy plików   //prawdopodobnie zbędne 
            $this->Image_model->setImage($this->getFolderName(), $filenameNew);
            $filedetail['folder'] = $this->getFolderName();
            $filedetail['filename'] = $filenameNew;
            $filedetail['dimensions'] =  $this->Image_model->getDimensions();
            $filesWithDetails[] = $filedetail;
        }
        return $filesWithDetails;
    }

    public function uploadFiles()
    {
        //var_dump($_FILES);
        //var_dump($_FILES['filesToUpload']);return;
         if(count($_FILES['filesToUpload']['name'])) {
             for($i = 0; $i < count($_FILES['filesToUpload']['name']); $i++) {
                move_uploaded_file($_FILES['filesToUpload']["tmp_name"][$i], $this->getFolderFullPath() . $this->sanitizeFileName($_FILES['filesToUpload']["name"][$i]));
            }
        }
        redirect(site_url() . "folder/view/". $this->getFolderName());
    }
}
