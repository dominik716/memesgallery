# Galeria

Lekka, responsywna galeria do przeglądania zdjęć zapisanych na serwerze.  
Wykorzystano framework PHP: Codeigniter  
Wykorzystano framework CSS: MDBootstrap 

## [PoC Video](docs/gallery-visualisation.mp4)

#### [Demo](http://domcio.epizy.com/memes/)

<img src="docs/gallery-visualisation.gif" alt="drawing"/>


![foldery](git/gallery-main.png)
![foldery](git/gallery-folder-preview2.gif)

## Zalety
 - pełna responsywność
  
  <img src="git/mobile.png" alt="drawing" width="300"/>

  - upload plików
![foldery](git/gallery-upload.gif)


- tworzenie folderów
![foldery](git/gallery-folder-creation.gif)


